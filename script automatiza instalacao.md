## Este script automatiza o processo de instalação do Docker e Docker-compose no sistema .deb.

Este script automatiza o processo de instalação do Docker no sistema Ubuntu. Ele faz o download da chave GPG pública do repositório oficial do Docker, verifica a autenticidade dos pacotes baixados e os instala no sistema. O Docker é uma plataforma popular de software livre usada para desenvolver, enviar e executar aplicativos em contêineres.

```shell
vim install_docker.sh
```

```shell
#!/bin/bash

# Obtém o código do nome da versão do Ubuntu.
CODENAME=$(grep VERSION_CODENAME /etc/os-release | cut -d '=' -f 2)

# responsável pela atualização do respositorios.
apt update

# pacotes necessários para o docker.
apt install -y apt-transport-https ca-certificates curl software-properties-common

# Baixa a chave GPG pública usada para verificar os pacotes do repositório Docker e Descriptografa a chave GPG baixada e a converte para um formato legível.
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $CODENAME stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# responsável pela atualização do respositorios.
apt update

# Instalação do Docker
apt-get install -y docker.io

# instalação do docker-compose
wget https://github.com/docker/compose/releases/download/v2.27.1/docker-compose-linux-x86_64

# renomeando o diretorido/arquivo
mv docker-compose-linux-x86_64 docker-compose

# movendo o docker-compose para o /bin
mv docker-compose /bin/

# tornando executavel.
chmod +x /bin/docker-compose

# verificando versao do docker e docker-compose
docker-compose --version
docker --version
```
```shell
chmod +x install_docker.sh
./install_docker.sh
```
### Criando container no docker e docker-compose

## docker
O comando docker run é usado para criar e executar um contêiner com base em uma imagem. No exemplo abaixo, estamos criando um contêiner chamado "ubuntu2023" com o sistema operacional Ubuntu. O argumento "-dit" está sendo usado para iniciar o contêiner em segundo plano, enquanto o argumento "--name" define o nome do contêiner. Posteriormente, o comando "docker exec -it -uroot" é usado para acessar o shell do contêiner em execução como o usuário root.
```shell
docker run -dit --name ubuntu2023 ubuntu
```
Verifique o status do container.
```shell
docker ps
```
Acessando o container com **Root**
```shell
docker exec -it -uroot ubuntu2023 bash
```

## Arquivo Docker Compose

O arquivo docker-compose.yml é usado para definir e executar aplicativos Docker compostos por vários contêineres. No exemplo fornecido, estamos definindo um serviço chamado "apache" usando a imagem "php:7.4-apache". O serviço está configurado para reiniciar automaticamente, expor o contêiner na porta 80 do host e mapear o diretório "./html" do host para o diretório "/var/www/html" do contêiner.
```shell
version: '3'

services:
  apache:
    image: 'php:7.4-apache'
    container_name: php
    restart: always
    ports:
      - '80:80'
    volumes:
      - ./html:/var/www/html
```
Verifique o status do container.
```shell
docker ps
```
vamos criar um arquivo index.html que vai ficar disponivel.
```shell
vim html/index.html
```
```html
<!DOCTYPE html>
<html>
<head>
    <title>Com linux</title>
</head>
<body>
    <h1>Curte comentar e compatilha nosso canal "Com linux"</h1>
</html>
```
### Acesso ao Servidor Apache:
Após a configuração do serviço Apache, o servidor está acessível em "192.168.100.125:80". O conteúdo do servidor pode ser visualizado no navegador da web ou acessado usando o comando curl. Um exemplo de página HTML básica está disponível no endereço "192.168.100.125:80/index.html".
